﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Lottie.Forms.Droid;
using Plugin.CurrentActivity;
using Xamarin;
using Xamarin.Forms;

namespace Demeterre.Droid
{
    [Activity(Label = "Demeterre", Icon = "@drawable/DemeterreIcon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            global::Xamarin.Forms.FormsMaterial.Init(this, savedInstanceState);

            // Nécessaire pour la géolocalisation
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;

            string[] permissionsNeeded = new string[] { Manifest.Permission.AccessCoarseLocation,
                                                        Manifest.Permission.AccessFineLocation,
                                                        Manifest.Permission.AccessLocationExtraCommands,
                                                        Manifest.Permission.AccessNetworkState,
                                                        Manifest.Permission.AccessWifiState,
                                                        Manifest.Permission.WriteExternalStorage
            };
            for (int i = 0; i < permissionsNeeded.Length; i++)
            {
                if (ContextCompat.CheckSelfPermission(this, permissionsNeeded[i]) != Permission.Granted)
                {
                    // Si une des permissions de la lsite n'est pas déjà autorisée, on interoge l'utilisateur
                    ActivityCompat.RequestPermissions(this, permissionsNeeded, 1);
                    break;
                }
            }

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            //Iconize
            Plugin.Iconize.Iconize.Init(TabLayoutResource, ToolbarResource);
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeRegularModule())
                .With(new Plugin.Iconize.Fonts.FontAwesomeBrandsModule())
                .With(new Plugin.Iconize.Fonts.FontAwesomeSolidModule());

            Xamarin.FormsMaps.Init(this, savedInstanceState);

            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}