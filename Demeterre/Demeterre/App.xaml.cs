﻿using Demeterre.Codes.Utility;
using Demeterre.Views;
using Xamarin.Forms;

namespace Demeterre
{
    public partial class App : Application
    {
        private static ViewModelLocator _Locator;
        public static ViewModelLocator Locator
        {
            get { return _Locator ?? (_Locator = new ViewModelLocator()); }
        }


        public App()
        {
            InitializeComponent();
            var myColors = this.Resources.Values;
            MainPage = new NavigationPage(new ConnexionPage());

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
