﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Codes.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SuccessView : ContentView
    {
        public SuccessView()
        {
            InitializeComponent();
        }
    }
}