﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre.Codes.Services
{
    public interface ICloseAppHelper {
        void CloseApp();
    }
}
