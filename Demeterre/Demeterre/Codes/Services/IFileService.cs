﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Demeterre.Codes.Services
{
    public interface IFileService
    {
        void SaveText(string filename, string text);
        Task<string> LoadText(string filename);
        bool OpenPdf(string filePath);
        Task<string> SavePdf_Android(string fileName, Byte[] data);
        void SavePdf_Ios(string filePath, byte[] data);
        Task<bool> OpenPdfWindows(string filePath);
    }

}
