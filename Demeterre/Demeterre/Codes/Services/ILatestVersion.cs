﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Demeterre.Codes.Services
{
    public interface ILatestVersion {
        /// <inheritdoc />
        Task<string> GetLatestVersionNumber(string appName);
        Task OpenAppInStore(string appName);
    }
}
