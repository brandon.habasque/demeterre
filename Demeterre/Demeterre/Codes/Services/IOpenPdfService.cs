﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre.Codes.Services
{
    public interface IOpenPdfService
    {
        bool OpenPdf(string filePath);
    }
}
