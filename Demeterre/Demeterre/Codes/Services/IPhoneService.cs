﻿
namespace Demeterre.Codes.Services
{
    public interface IPhoneService {

        void Sms(string number, string message);
        void Call(string person, string number);
        void Email(string to, string cc, string sujet, string corps);
        void GpsCoordonnees(string latitude, string longitude);
        void GpsAdresse(string adresse);
        void OpneLink(string lien);

    }
}
