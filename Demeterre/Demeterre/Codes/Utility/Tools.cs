﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.IO;
using Demeterre.Codes.Services;
using Demeterre_Library;
using Demeterre_Library.Models;

namespace Demeterre.Codes.Utility
{
    public static class Tools
    {
        public static string UrlServeurProd { get; set; } = UrlServeurEloise;

        public static string UrlServeurBrandon { get { return "https://localhost:44326/"; } }

        public static string UrlServeurEloise { get { return "http://192.168.1.45/DemeterreAPI/"; } }

        //Data
        static public string AndroidPackage { get { return "fr.mds.demeterre"; } }
        static public string AppleId { get { return "1111111111"; } }
        static public string Version
        {
            get
            {
                Xamarin.Essentials.VersionTracking.Track();
                return Xamarin.Essentials.VersionTracking.CurrentVersion;
            }
        }

        public static string MyTocken { get; set; }

        //Data
        private static DownloadMobileContext _dc;
        public static DownloadMobileContext dc
        {
            get
            {
                if (_dc == null)
                    _dc = new DownloadMobileContext();
                return _dc;
            }
            set { if (_dc != value) { _dc = value; } }
        }

        //public static async Task<string> SynchroData(string Code, string Mdp, bool IsInit)
        //{
        //    string ErrSynchro = string.Empty;

        //    try
        //    {

        //        //Test la connectivité
        //        if (Xamarin.Essentials.Connectivity.NetworkAccess != Xamarin.Essentials.NetworkAccess.Internet)
        //        {
        //            throw new Exception("Pas de connexion internet");
        //        }

        //        //Prepare les datas à uploader
        //        string strUpload = "";
        //        if (!IsInit) strUpload = JsonConvert.SerializeObject(await PrepareData());
        //        else strUpload = JsonConvert.SerializeObject(new UploadMobileContext());

        //        //Synchro
        //        using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
        //        {
        //            client.BaseAddress = new Uri(Tools.UrlServeurProd);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
        //            client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
        //            client.Timeout = TimeSpan.FromMilliseconds(300000);

        //            client.DefaultRequestHeaders.Add("para1", Code);
        //            client.DefaultRequestHeaders.Add("para2", Mdp);

        //            client.DefaultRequestHeaders.Add("para4", Tools.MyTocken);
        //            client.DefaultRequestHeaders.Add("para5", (Device.RuntimePlatform == Device.Android) ? "AND" : "IOS");
        //            client.DefaultRequestHeaders.Add("para6", Tools.Version);

        //            var result = await client.PostAsync("Mobile/MobileContext", new StringContent(strUpload, Encoding.UTF8, "application/json"));
        //            result.EnsureSuccessStatusCode();
        //            var tmpContext = JsonConvert.DeserializeObject<DownloadMobileContext>(await result.Content.ReadAsStringAsync());

        //            if (!string.IsNullOrEmpty(tmpContext.messageRetour)) ErrSynchro = tmpContext.messageRetour;
        //            else
        //            {
        //                dc = tmpContext;
        //                await Tools.SaveData();
        //            }
        //            tmpContext = null;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrSynchro = "Une erreur est survenue lors de la synchronisation : " + ex.Message;
        //    }

        //    return ErrSynchro;
        //}

        //public static async Task<bool> LoadData(string fileToload = null)
        //{

        //    string msgErr = string.Empty;

        //    IFolder rootFolder = FileSystem.Current.LocalStorage;
        //    IFolder folder = await rootFolder.CreateFolderAsync("DemeterreData", CreationCollisionOption.OpenIfExists);
        //    foreach (PropertyInfo propertyInfo in dc.GetType().GetRuntimeProperties().Where(o => o.Name == (fileToload == null ? o.Name : fileToload)))
        //    {

        //        var isExistFile = await folder.CheckExistsAsync(propertyInfo.Name + ".txt");

        //        try
        //        {
        //            IFile file = isExistFile == ExistenceCheckResult.FileExists ? await folder.GetFileAsync(propertyInfo.Name + ".txt") : null;

        //            if (file != null)
        //            {

        //                if (propertyInfo.GetValue(dc) == null)
        //                {

        //                    try
        //                    {

        //                        if (propertyInfo.Name == "produit") propertyInfo.SetValue(dc, JsonConvert.DeserializeObject<ProduitMob>(await file.ReadAllTextAsync()));
        //                        else if (propertyInfo.Name == "Tiers") propertyInfo.SetValue(dc, JsonConvert.DeserializeObject<TiersMob>(await file.ReadAllTextAsync()));

        //                        if (propertyInfo.GetValue(dc) == null) propertyInfo.SetValue(dc, Activator.CreateInstance(propertyInfo.PropertyType));

        //                    }
        //                    catch (Exception e)
        //                    {
        //                        msgErr += e.Message + Environment.NewLine;
        //                        propertyInfo.SetValue(dc, Activator.CreateInstance(propertyInfo.PropertyType));
        //                    }

        //                }
        //            }
        //            else propertyInfo.SetValue(dc, Activator.CreateInstance(propertyInfo.PropertyType));

        //        }
        //        catch (Exception ex)
        //        {
        //            msgErr += ex.Message + Environment.NewLine;
        //        }

        //    }

        //    return true;
        //}
        //public static async Task SaveData(string fileToSave = null)
        //{

        //    IFolder rootFolder = FileSystem.Current.LocalStorage;
        //    IFolder folder = await rootFolder.CreateFolderAsync("DemeterreData", CreationCollisionOption.OpenIfExists);

        //    foreach (PropertyInfo propertyInfo in dc.GetType().GetRuntimeProperties().Where(o => o.Name == (fileToSave == null ? o.Name : fileToSave)))
        //    {
        //        if (propertyInfo.Name != "messageRetour")
        //        {
        //            IFile file = await folder.CreateFileAsync(propertyInfo.Name + ".txt", CreationCollisionOption.ReplaceExisting);
        //            await file.WriteAllTextAsync(JsonConvert.SerializeObject(propertyInfo.GetValue(dc)));
        //        }
        //    }

        //}
        //public static async Task<UploadMobileContext> PrepareData()
        //{
        //    UploadMobileContext myUpload = new UploadMobileContext();

        //    return myUpload;
        //}

        ////Cryptage MD5
        //public static string GetMD5hash(string data)
        //{
        //    data = "optav!s" + data;

        //    IHashAlgorithmProvider algoProv = PCLCrypto.WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Md5);
        //    byte[] dataB = Encoding.UTF8.GetBytes(data);
        //    byte[] dataHash = algoProv.HashData(dataB);
        //    var hex = new StringBuilder(dataHash.Length * 2);
        //    foreach (byte b in dataHash)
        //    {
        //        hex.AppendFormat("{0:x2}", b);
        //    }
        //    return hex.ToString();
        //}

        //Read-Write property
        public static void WriteProperty(string key, object value)
        {
            if (Application.Current != null)
            {
                Application.Current.Properties[key] = value;
                Application.Current.SavePropertiesAsync();
            }
        }

        public static string ReadProperty(string key)
        {
            if (Application.Current != null && Application.Current.Properties != null && Application.Current.Properties.ContainsKey(key) && Application.Current.Properties[key] != null)
                return Application.Current.Properties[key].ToString();
            else return null;
        }

        public static void ClearProperties()
        {
            Application.Current.Properties.Clear();
            Application.Current.SavePropertiesAsync();
        }

        //Gestion Sermaine / Année
        public static int GetWeek(System.DateTime time)
        {

            var rule = E_CALENDARWEEKRULES.CWR_FIRSTFOURDAYWEEK;
            var firstDayOfWeek = E_WEEKDAY.WD_MONDAY;


            int lReturn = 0;
            System.DateTime dtBase = new System.DateTime(time.Year, 1, 1);
            System.DateTime dtBuff = default(System.DateTime);
            int lDay = 0;
            int lWeek = 0;
            switch (rule)
            {
                case E_CALENDARWEEKRULES.CWR_FIRSTDAY:
                    for (lDay = 0; lDay <= 6; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek)
                        {
                            break;
                        }
                    }
                    if ((E_WEEKDAY)dtBase.DayOfWeek == firstDayOfWeek)
                    {
                        lReturn = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 1;
                    }
                    else
                    {
                        lReturn = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 2;
                    }
                    break;
                case E_CALENDARWEEKRULES.CWR_FIRSTFULLWEEK:
                    for (lDay = 0; lDay <= 6; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek)
                        {
                            break;
                        }
                    }
                    lWeek = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 1;
                    if (lWeek == 0)
                    {
                        lReturn = GetWeek(new System.DateTime(time.Year - 1, 12, 31));
                    }
                    else
                    {
                        lReturn = lWeek;
                    }
                    break;
                case E_CALENDARWEEKRULES.CWR_FIRSTFOURDAYWEEK:
                    for (lDay = 0; lDay <= 12; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek & dtBuff.DayOfYear >= 5)
                        {
                            break;
                        }
                    }
                    lWeek = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 2;
                    if (lWeek == 0)
                    {
                        lReturn = GetWeek(new System.DateTime(time.Year - 1, 12, 31));
                    }
                    else
                    {
                        lReturn = lWeek;
                    }
                    break;
            }
            return lReturn;
        }
        public enum E_CALENDARWEEKRULES
        {
            CWR_FIRSTDAY = 0,
            CWR_FIRSTFULLWEEK = 1,
            CWR_FIRSTFOURDAYWEEK = 2
        }
        public enum E_WEEKDAY
        {
            WD_SUNDAY = 0,
            WD_MONDAY = 1,
            WD_TUESDAY = 2,
            WD_WEDNESDAY = 3,
            WD_THURSDAY = 4,
            WD_FRIDAY = 5,
            WD_SATURDAY = 6
        }
        public static int getWeekOfYear(System.DateTime time)
        {
            E_WEEKDAY yDay = (E_WEEKDAY)time.DayOfWeek;
            System.DateTime dtBuff = time;
            if (yDay >= E_WEEKDAY.WD_MONDAY & yDay <= E_WEEKDAY.WD_WEDNESDAY)
            {
                dtBuff = dtBuff.AddDays(3);
            }
            return GetWeek(dtBuff);
        }

        public static int GetAnnee(System.DateTime time)
        {

            var myJour = getWeekOfYear(time);
            var myAnnee = time.Year;

            if (myJour == 1 && time.Month == 12) myAnnee += 1;

            return myAnnee;
        }
        public static DateTime FirstDateOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            int firstWeek = GetIso8601WeekOfYear(firstThursday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        private static int GetIso8601WeekOfYear(System.DateTime time)
        {
            E_WEEKDAY yDay = (E_WEEKDAY)time.DayOfWeek;
            System.DateTime dtBuff = time;
            if (yDay >= E_WEEKDAY.WD_MONDAY & yDay <= E_WEEKDAY.WD_WEDNESDAY)
            {
                dtBuff = dtBuff.AddDays(3);
            }
            return GetWeekOfYear(dtBuff, E_CALENDARWEEKRULES.CWR_FIRSTFOURDAYWEEK, E_WEEKDAY.WD_MONDAY);
        }
        private static int GetWeekOfYear(System.DateTime time, E_CALENDARWEEKRULES rule, E_WEEKDAY firstDayOfWeek)
        {
            int lReturn = 0;
            System.DateTime dtBase = new System.DateTime(time.Year, 1, 1);
            System.DateTime dtBuff = default(System.DateTime);
            int lDay = 0;
            int lWeek = 0;
            switch (rule)
            {
                case E_CALENDARWEEKRULES.CWR_FIRSTDAY:
                    for (lDay = 0; lDay <= 6; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek)
                        {
                            break;
                        }
                    }

                    if ((E_WEEKDAY)dtBase.DayOfWeek == firstDayOfWeek)
                    {
                        lReturn = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 1;
                    }
                    else
                    {
                        lReturn = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 2;
                    }
                    break;
                case E_CALENDARWEEKRULES.CWR_FIRSTFULLWEEK:
                    for (lDay = 0; lDay <= 6; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek)
                        {
                            break;
                        }
                    }

                    lWeek = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 1;
                    if (lWeek == 0)
                    {
                        lReturn = GetWeekOfYear(new System.DateTime(time.Year - 1, 12, 31), E_CALENDARWEEKRULES.CWR_FIRSTFULLWEEK, firstDayOfWeek);
                    }
                    else
                    {
                        lReturn = lWeek;
                    }
                    break;
                case E_CALENDARWEEKRULES.CWR_FIRSTFOURDAYWEEK:
                    for (lDay = 0; lDay <= 12; lDay++)
                    {
                        dtBuff = dtBase.AddDays(lDay);
                        if ((E_WEEKDAY)dtBuff.DayOfWeek == firstDayOfWeek & dtBuff.DayOfYear >= 5)
                        {
                            break;
                        }
                    }

                    lWeek = (int)System.Math.Floor(((double)time.DayOfYear - (double)dtBuff.DayOfYear) / 7D) + 2;
                    if (lWeek == 0)
                    {
                        lReturn = GetWeekOfYear(new System.DateTime(time.Year - 1, 12, 31), E_CALENDARWEEKRULES.CWR_FIRSTFOURDAYWEEK, firstDayOfWeek);
                    }
                    else
                    {
                        lReturn = lWeek;
                    }
                    break;
            }
            return lReturn;
        }

        //Fonctions Divers
        public static bool IsAppInit
        {
            get { return !string.IsNullOrEmpty(Tools.dc.Tiers.MailTiers); }
        }

        public static async Task<Boolean> IsPasswordOK(string Code, string Mdp)
        {
            bool IsPwdOK = false;

            try
            {
                //Synchro
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Tools.UrlServeurProd);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("para1", Code);
                    client.DefaultRequestHeaders.Add("para2", Mdp);

                    var result = await client.GetAsync("Mobile/IsPwdOK");
                    result.EnsureSuccessStatusCode();
                    var tmpContext = JsonConvert.DeserializeObject<DownloadMobileContext>(await result.Content.ReadAsStringAsync());

                    if (string.IsNullOrEmpty(tmpContext.messageRetour)) IsPwdOK = true;

                }

            }
            catch (Exception)
            {
                IsPwdOK = false;
            }

            return IsPwdOK;

        }

        public static string Left(String str, int Digit)
        {

            if (string.IsNullOrEmpty(str)) return "";
            if (Digit > str.Length)
            {
                return str;
            }
            else
            {
                return str.Substring(0, Digit);
            }

        }

        public static string Right(String str, int Digit)
        {

            if (string.IsNullOrEmpty(str)) return "";
            int deb = str.Length - Digit;
            if (deb < 0)
            {
                return str;
            }
            else
            {
                return str.Substring(deb, Digit);
            }
        }

        //public static async Task<string> GetFile(string TypeFile, string Id)
        //{

        //    string ErrSynchro = string.Empty;

        //    try
        //    {

        //        if (Xamarin.Essentials.Connectivity.NetworkAccess != Xamarin.Essentials.NetworkAccess.Internet) throw new Exception("Pas de connexion internet.");

        //        IFolder rootFolder = FileSystem.Current.LocalStorage;
        //        IFolder folder = await rootFolder.CreateFolderAsync("TMP", CreationCollisionOption.OpenIfExists);

        //        bool IsOKToOpen = true;

        //        using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
        //        {
        //            client.BaseAddress = new Uri(Tools.UrlServeurProd);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
        //            client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
        //            client.Timeout = TimeSpan.FromMilliseconds(300000);

        //            client.DefaultRequestHeaders.Add("para1", Tools.dc.Tiers.MailTiers);
        //            client.DefaultRequestHeaders.Add("para2", Tools.ReadProperty("PWD"));

        //            if (TypeFile == "DOC")
        //            {
        //                client.DefaultRequestHeaders.Add("nomdoc", Id);
        //                Id = "0";
        //            }

        //            var myFileByte = await client.GetByteArrayAsync("Mobile/GetFile/" + TypeFile + "/" + Id);

        //            if (myFileByte != null)
        //            {
        //                var myFile = await folder.CreateFileAsync(TypeFile + ".pdf", CreationCollisionOption.ReplaceExisting);
        //                File.WriteAllBytes(myFile.Path, myFileByte);
        //            }
        //            else IsOKToOpen = false;

        //        }

        //        IFile file = null;
        //        if (IsOKToOpen) file = await folder.GetFileAsync(TypeFile + ".pdf");
        //        if (file != null)
        //        {
        //            DependencyService.Get<IOpenPdfService>().OpenPdf(file.Path);
        //        }
        //        else
        //        {
        //            throw new Exception("Impossible d'ouvrir le fichier.");
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrSynchro = "Une erreur est survenue : " + ex.Message;
        //    }

        //    return ErrSynchro;
        //}

        public static async Task<DownloadMobileContext> GetOnLineContext(string TypeToLoad, int IdRef1, int IdRef2, string Commentaire)
        {

            DownloadMobileContext myContext = new DownloadMobileContext();

            try
            {

                //Test la connectivité
                if (Xamarin.Essentials.Connectivity.NetworkAccess != Xamarin.Essentials.NetworkAccess.Internet)
                {
                    throw new Exception("Pas de connexion internet");
                }

                //Synchro
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(Tools.UrlServeurProd);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
                    client.Timeout = TimeSpan.FromMilliseconds(300000);

                    client.DefaultRequestHeaders.Add("para1", Tools.dc.Tiers.MailTiers);
                    client.DefaultRequestHeaders.Add("para2", Tools.ReadProperty("PWD"));
                    client.DefaultRequestHeaders.Add("Commentaire", Commentaire);

                    var result = await client.GetAsync("Mobile/OnLineContext/" + TypeToLoad + "/" + IdRef1.ToString() + "/" + IdRef2.ToString());
                    result.EnsureSuccessStatusCode();
                    myContext = JsonConvert.DeserializeObject<DownloadMobileContext>(await result.Content.ReadAsStringAsync());

                }

            }
            catch (Exception ex)
            {
                myContext.messageRetour = "Une erreur est survenue lors de la synchronisation : " + ex.Message;
            }

            return myContext;
        }

        public static decimal? Get2DigitFrom(decimal? source)
        {
            return source.HasValue ? decimal.Round(source.Value, 2, MidpointRounding.AwayFromZero) : new decimal?();
        }
    }
}
