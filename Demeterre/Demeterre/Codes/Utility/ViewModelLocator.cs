﻿using Demeterre.ViewModels;
using Demeterre.Views;
using GalaSoft.MvvmLight.Ioc;

namespace Demeterre.Codes.Utility
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            SimpleIoc.Default.Register<BaseViewModel>();
            SimpleIoc.Default.Register<ConnexionViewModel>();
            SimpleIoc.Default.Register<AccueilViewModel>();
            SimpleIoc.Default.Register<ParametresViewModel>();
            SimpleIoc.Default.Register<EditProfilViewModel>();
            SimpleIoc.Default.Register<PanierViewModel>();
            SimpleIoc.Default.Register<RecapPanierViewModel>();
            SimpleIoc.Default.Register<ProducerHomeViewModel>();
            SimpleIoc.Default.Register<DetailElementPourPanierViewModel>();

        }

        public BaseViewModel BaseVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<BaseViewModel>();
            }
        }

        public ConnexionViewModel ConnexionVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ConnexionViewModel>();
            }
        }

        public AccueilViewModel AccueilVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<AccueilViewModel>();
            }
        }

        public ParametresViewModel ParametresVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ParametresViewModel>();
            }
        }

        public EditProfilViewModel EditProfilVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<EditProfilViewModel>();
            }
        }

        public PanierViewModel PanierVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<PanierViewModel>();
            }
        }

        public RecapPanierViewModel RecapPanierVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<RecapPanierViewModel>();
            }
        }

        public ProducerHomeViewModel ProducerHomeVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ProducerHomeViewModel>();
            }
        }

        public DetailElementPourPanierViewModel DetailElementPourPanierVM
        {
            get
            {
                return SimpleIoc.Default.GetInstance<DetailElementPourPanierViewModel>();
            }
        }
    }
}
