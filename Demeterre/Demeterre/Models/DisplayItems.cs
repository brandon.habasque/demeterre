﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre.Models
{
    public class DisplayItems : ViewModelBase
    {
        public int ID { get; set; }
        public string Lib1 { get; set; }
        public string Lib2 { get; set; }
        public string Lib3 { get; set; }
        public string Lib4 { get; set; }
        public string Lib5 { get; set; }
        public string Lib6 { get; set; }
        public string Lib7 { get; set; }
        public string Lib8 { get; set; }
        public string Lib9 { get; set; }
        public string Lib10 { get; set; }
        public string Lib11 { get; set; }
        public string Lib12 { get; set; }
        public string Lib13 { get; set; }
        public string Lib14 { get; set; }
        public string Lib15 { get; set; }
        public string Lib16 { get; set; }
        public string Lib17 { get; set; }
        public string Lib18 { get; set; }
        public string Lib19 { get; set; }
        public string Lib20 { get; set; }
        public int Num1 { get; set; }
    }
}
