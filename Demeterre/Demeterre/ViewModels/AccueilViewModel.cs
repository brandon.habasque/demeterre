﻿using Demeterre.Codes.Utility;
using Demeterre.Models;
using Demeterre.Views;
using Demeterre_Library.Models;
using GalaSoft.MvvmLight.Command;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Position = Xamarin.Forms.Maps.Position;

namespace Demeterre.ViewModels
{
    public class AccueilViewModel : BaseViewModel
    {
        #region Properties
        private string _ProduitSemaine;
        public string ProduitSemaine
        {
            get { return _ProduitSemaine; }
            set { _ProduitSemaine = value; RaisePropertyChanged("ProduitSemaine"); }
        }

        private string _SearchValue;
        public string SearchValue {
            get { return _SearchValue; }
            set { _SearchValue = value; RaisePropertyChanged("SearchValue"); }
        }
        
        private ObservableCollection<DisplayItems> _TypeProduitList;
        public ObservableCollection<DisplayItems> TypeProduitList
        {
            get { return _TypeProduitList; }
            set { _TypeProduitList = value; RaisePropertyChanged("TypeProduitList"); }
        }

        private List<ProduitMob> _ListProds;
        public List<ProduitMob> ListProds
        {
            get { return _ListProds; }
            set { _ListProds = value; RaisePropertyChanged("ListProds"); }
        }

        private Pin _SelectedPin;
        public Pin SelectedPin {
            get { return _SelectedPin; }
            set { _SelectedPin = value; RaisePropertyChanged("SelectedPin"); }
        }

        //IsBioRapide
        private bool _IsTous;
        public bool IsTous
        {
            get { return _IsTous; }
            set { _IsTous = value; RaisePropertyChanged("IsTous"); }
        }

        private bool _IsBox;
        public bool IsBox
        {
            get { return _IsBox; }
            set { _IsBox = value; RaisePropertyChanged("IsBox"); }
        }

        private bool _IsFruit;
        public bool IsFruit
        {
            get { return _IsFruit; }
            set { _IsFruit = value; RaisePropertyChanged("IsFruit"); }
        }

        private bool _IsLegume;
        public bool IsLegume
        {
            get { return _IsLegume; }
            set { _IsLegume = value; RaisePropertyChanged("IsLegume"); }
        }

        private bool _IsSelectedProd;
        public bool IsSelectedProd {
            get { return _IsSelectedProd; }
            set { _IsSelectedProd = value; RaisePropertyChanged("IsSelectedProd"); }
        }

        private ObservableCollection<DisplayItems> _ProductList;
        public ObservableCollection<DisplayItems> ProductList
        {
            get { return _ProductList; }
            set { _ProductList = value; RaisePropertyChanged("ProductList"); }
        }

        private ObservableCollection<DisplayItems> _ProductDisplayedList;
        public ObservableCollection<DisplayItems> ProductDisplayedList
        {
            get { return _ProductDisplayedList; }
            set { _ProductDisplayedList = value; RaisePropertyChanged("ProductDisplayedList"); }
        }

        private ObservableCollection<DisplayItems> _ProduitsPanier;
        public ObservableCollection<DisplayItems> ProduitsPanier
        {
            get { return _ProduitsPanier; }
            set { _ProduitsPanier = value; RaisePropertyChanged("ProduitsPanier"); }
        }

        private Position _loc;
        public Position Loc
        {
            get { return _loc; }
            set { if (_loc != value) { _loc = value; RaisePropertyChanged("Loc"); } }
        }

        private DisplayItems _SelectedProduit;
        public DisplayItems SelectedProduit
        {
            get { return _SelectedProduit; }
            set {_SelectedProduit = value; if (value != null)
                {
                    this.GoToDetailProdAction(_SelectedProduit);
                } RaisePropertyChanged("Loc"); }
        }

        private Position MyPosition { get; set; }

        private ObservableCollection<Pin> _PinCollection = new ObservableCollection<Pin>();
        public ObservableCollection<Pin> PinCollection
        {
            get { return _PinCollection; }
            set
            { if (_PinCollection != value) { _PinCollection = value; RaisePropertyChanged("PinCollection"); } }
        }
        #endregion

        #region Command

        private RelayCommand _OpenTousCommand;
        public RelayCommand OpenTousCommand
        {
            get
            {
                return _OpenTousCommand ?? (_OpenTousCommand = new RelayCommand(OpenTousAction));
            }
        }        

        private RelayCommand _OpenBoxCommand;
        public RelayCommand OpenBoxCommand
        {
            get
            {
                return _OpenBoxCommand ?? (_OpenBoxCommand = new RelayCommand(OpenBoxAction));
            }
        }

        private RelayCommand _OpenFruitCommand;
        public RelayCommand OpenFruitCommand
        {
            get
            {
                return _OpenFruitCommand ?? (_OpenFruitCommand = new RelayCommand(OpenFruitAction));
            }
        }

        private RelayCommand _OpenLegumeCommand;
        public RelayCommand OpenLegumeCommand
        {
            get
            {
                return _OpenLegumeCommand ?? (_OpenLegumeCommand = new RelayCommand(OpenLegumeAction));
            }
        }

        private RelayCommand<DisplayItems> _AddProductCommand;
        public RelayCommand<DisplayItems> AddProductCommand
        {
            get
            {
                return _AddProductCommand ?? (_AddProductCommand = new RelayCommand<DisplayItems>((t)=>AddProductAction(t)));
            }
        }

        private RelayCommand<DisplayItems> _RemoveProductCommand;
        public RelayCommand<DisplayItems> RemoveProductCommand
        {
            get
            {
                return _RemoveProductCommand ?? (_RemoveProductCommand = new RelayCommand<DisplayItems>((t) => RemoveProductAction(t)));
            }
        }

        private RelayCommand<string> _RaccourciCommand;
        public RelayCommand<string> RaccourciCommand {
            get {
                return _RaccourciCommand ?? (_RaccourciCommand = new RelayCommand<string>((t) => RaccourciAction(t)));
            }
        }

        #endregion

        #region Methods
        public AccueilViewModel()
        {
            
        }

        public async void InitView()
        {
            this.IsBusy = true;

            //await Tools.LoadData("produit");

            //var produits = Tools.dc.Produits.ToList();

            this.IsAccueil = true;
            this.IsBioRapide = false;
            this.IsParametre = false;
            this.IsPanier = false;
            this.IsSearch = false;
            this.IsSelectedProd = false;
            //this.IsTous = true;
            //this.IsBox = false;
            //this.IsFruit = false;
            //this.IsLegume = false;
            this.GetSeason();

            this.LoadData();

            this.AccueilAction();

            this.IsBusy = false;
        }

        private async void LoadData()
        {
            App.Locator.ParametresVM.InitView();
            App.Locator.PanierVM.InitView();

            //this.ListProds = new List<ProduitMob>((from o in Tools.dc.Produits
            //                                       select new ProduitMob()
            //                                       {
            //                                           IDProduit = o.IDProduit,
            //                                           LibelleProduit = o.LibelleProduit,
            //                                           QuantiteStock = o.QuantiteStock,
            //                                           QuantiteVendu = o.QuantiteVendu

            //                                       }).ToList());
            Geocoder geocoder = new Geocoder();
            //GetPosition();

            var approximateLocations = await geocoder.GetPositionsForAddressAsync("Le Clos Renaud + 35170 + BRUZ");
            var pos1 = approximateLocations.FirstOrDefault();
            approximateLocations = await geocoder.GetPositionsForAddressAsync("Coutance + 35650 + Le Rheu");
            var pos2 = approximateLocations.FirstOrDefault();
            approximateLocations = await geocoder.GetPositionsForAddressAsync("Lesnelay + 35310 + Chavagne");
            var pos3 = approximateLocations.FirstOrDefault();
            if (pos1 != null)
            {
                this.Loc = new Position(48.048590, -1.742194);
                PinCollection.Add(new Pin()
                {
                    Position = pos1,
                    Type = PinType.Place,
                    Label = "Au marais sage",
                });
                PinCollection.Add(new Pin() {
                    Position = pos2,
                    Type = PinType.Place,
                    Label = "Le Rheu Maraîchers",
                });
                PinCollection.Add(new Pin() {
                    Position = pos3,
                    Type = PinType.Place,
                    Label = "Jean-Martial et Julien",
                });

                foreach (var item in PinCollection) {
                    item.InfoWindowClicked += async (s, args) =>
                    {
                        bool isOk = await Application.Current.MainPage.DisplayAlert(item.Label, "Souhaitez vous consulter les produits de ce producteur ?", "Oui", "Non");
                        if (isOk) {
                            this.IsSelectedProd = true;
                            this.SelectedPin = item;
                        }
                    };
                }
            }
            this.ProduitsPanier = App.Locator.PanierVM.ProduitsPanier;
            this.CountProdPanier = App.Locator.PanierVM.ProduitsPanier.Count;

            this.ProduitSemaine = "Tomate";

            this.TypeProduitList = new ObservableCollection<DisplayItems>()
            {
                new DisplayItems()
                {
                    ID = 0,
                    Lib1 = "#DD4040",
                    Lib2 = "box.png",
                    Lib3 = "Box",
                    Lib4 = "box"
                },
                new DisplayItems()
                {
                    ID = 1,
                    Lib1 = "#ffd958",
                    Lib2 = "favoris.png",
                    Lib3 = "Favoris",
                    Lib4 = "favoris"
                },
                new DisplayItems()
                {
                    ID = 2,
                    Lib1 = "#d2f475",
                    Lib2 = "preference.png",
                    Lib3 = "Préférences",
                    Lib4 = "preference"
                }
            };

            this.ProductList = new ObservableCollection<DisplayItems>()
            {
                new DisplayItems()
                {
                    Lib1 = "appleColor.png",
                    Lib2 = "Pomme",
                    Num1 = 0,
                    Lib3 = "1.00",
                    Lib4 = "fruit",
                    Lib5 = "50"
                },
                new DisplayItems()
                {
                    Lib1 = "kiwiColor.png",
                    Lib2 = "Kiwi",
                    Num1 = 0,
                    Lib3 = "1.50",
                    Lib4 = "fruit",
                    Lib5 = "20"
                },
                new DisplayItems()
                {
                    Lib1 = "TomateColor.png",
                    Lib2 = "Tomate",
                    Num1 = 0,
                    Lib4 = "légume",
                    Lib3 = "0.50",
                    Lib5 = "102"
                },
                new DisplayItems()
                {
                    Lib1 = "LaitueColor.png",
                    Lib2 = "Laitue",
                    Num1 = 0,
                    Lib3 = "3.00",
                    Lib4 = "légume",
                    Lib5 = "10"
                },
            };

            this.ProductDisplayedList = new ObservableCollection<DisplayItems>(this.ProductList);
        }

        public async void GetPosition()
        {
            Plugin.Geolocator.Abstractions.Position position = null;

            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    MyPosition = new Position(position.Latitude, position.Longitude);
                    return;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception ex)
            {
                var x = ex.StackTrace;
            }

            MyPosition = new Position(position.Latitude, position.Longitude);
            if (position == null)
                return;

        }

        private void OpenTousAction()
        {
            this.IsBusy = true;

            this.ProductDisplayedList = new ObservableCollection<DisplayItems>();
            foreach (DisplayItems item in this.ProductList)
            {
                if (item.Lib4 == "fruit" || item.Lib4 == "légume")
                {
                    this.ProductDisplayedList.Add(item);
                }
            }
            this.IsTous = true;
            this.IsBox = false;
            this.IsFruit = false;
            this.IsLegume = false;

            this.IsBusy = false;
        }

        private void OpenBoxAction()
        {
            this.IsBusy = true;

            //TO DO
            this.ProductDisplayedList = new ObservableCollection<DisplayItems>();
            this.IsTous = false;
            this.IsBox = true;
            this.IsFruit = false;
            this.IsLegume = false;

            this.IsBusy = false;
        }

        private void OpenFruitAction()
        {
            this.IsBusy = true;

            this.ProductDisplayedList = new ObservableCollection<DisplayItems>();
            foreach (DisplayItems item in this.ProductList)
            {
                if (item.Lib4 == "fruit")
                {
                    this.ProductDisplayedList.Add(item);
                }
            }
            this.IsTous = false;
            this.IsBox = false;
            this.IsFruit = true;
            this.IsLegume = false;

            this.IsBusy = false;
        }

        private void OpenLegumeAction()
        {
            this.IsBusy = true;

            this.ProductDisplayedList = new ObservableCollection<DisplayItems>();
            foreach (DisplayItems item in this.ProductList)
            {
                if (item.Lib4 == "légume")
                {
                    this.ProductDisplayedList.Add(item);
                }
            }
            this.IsTous = false;
            this.IsBox = false;
            this.IsFruit = false;
            this.IsLegume = true;

            this.IsBusy = false;
        }

        public void AddProductAction(DisplayItems item)
        {
            item.Num1++;
        }

        public void RemoveProductAction(DisplayItems item)
        {
            if (item.Num1 > 0)
            {
                item.Num1--;
            }
        }

        private async void RaccourciAction(string raccourci) {
            var msg = string.Empty;

            if (raccourci == "box") {
                msg = "Cette fonctionnalité vous permet d'adhérer à nos différentes offres de box";
            }
            if (raccourci == "favoris") {
                msg = "Cette fonctionnalité vous permet de définir vos producteurs favoris";
            }
            if (raccourci == "preference") {
                msg = "Cette fonctionnalité vous permet de définir vos préférences";
            }

            await Application.Current.MainPage.DisplayAlert(raccourci.ToUpper(), msg, "compris");
        }

        public async void GoToDetailProdAction(DisplayItems selectedProd)
        {
            App.Locator.DetailElementPourPanierVM.SelectedProduit = selectedProd;
            App.Locator.DetailElementPourPanierVM.InitView();
            this.IsBusy = true;
            Page targetPage = (Page)Activator.CreateInstance(typeof(DetailElementPourPanierPage));
            await Application.Current.MainPage.Navigation.PushAsync(targetPage);
            this.IsBusy = false;
        }

        #endregion
    }
}
