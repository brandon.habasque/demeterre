﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using Demeterre.Codes.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Demeterre.Views;

namespace Demeterre.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        #region Properties

        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        private bool _IsSuccess;
        public bool IsSuccess
        {
            get { return _IsSuccess; }
            set { _IsSuccess = value; RaisePropertyChanged("IsSuccess"); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private bool _IsProfil;
        public bool IsProfil
        {
            get { return _IsProfil; }
            set { _IsProfil = value; RaisePropertyChanged("IsProfil"); }
        }

        private bool _IsStudent;
        public bool IsStudent
        {
            get { return _IsStudent; }
            set { _IsStudent = value; RaisePropertyChanged("IsStudent"); }
        }

        private bool _IsProducer;
        public bool IsProducer
        {
            get { return _IsProducer; }
            set { _IsProducer = value; RaisePropertyChanged("IsProducer"); }
        }

        private bool _IsAccueil = true;
        public bool IsAccueil
        {
            get { return _IsAccueil; }
            set { _IsAccueil = value; RaisePropertyChanged("IsAccueil"); }
        }

        private bool _IsBioRapide;
        public bool IsBioRapide
        {
            get { return _IsBioRapide; }
            set { _IsBioRapide = value; RaisePropertyChanged("IsBioRapide"); }
        }

        private bool _IsParametre;
        public bool IsParametre
        {
            get { return _IsParametre; }
            set { _IsParametre = value; RaisePropertyChanged("IsParametre"); }
        }

        private bool _IsPanier;
        public bool IsPanier
        {
            get { return _IsPanier; }
            set { _IsPanier = value; RaisePropertyChanged("IsPanier"); }
        }

        private bool _IsSearch;
        public bool IsSearch {
            get { return _IsSearch; }
            set { _IsSearch = value; RaisePropertyChanged("IsSearch"); }
        }

        private int _NumCommande;
        public int NumCommande
        {
            get { return _NumCommande; }
            set { _NumCommande = value; RaisePropertyChanged("NumCommande"); }
        }

        private ParametresViewModel _Parametre;
        public ParametresViewModel Parametre
        {
            get { return _Parametre; }
            set { _Parametre = value; RaisePropertyChanged("Parametre"); }
        }

        private PanierViewModel _Panier;
        public PanierViewModel Panier
        {
            get { return _Panier; }
            set { _Panier = value; RaisePropertyChanged("Panier"); }
        }

        private int _CountProdPanier;
        public int CountProdPanier
        {
            get { return _CountProdPanier; }
            set { _CountProdPanier = value; RaisePropertyChanged("CountProdPanier"); }
        }

        //private string _MainColor = "#FDC1FB";
        //public string MainColor
        //{
        //    get { return _MainColor; }
        //    set { _MainColor = value; RaisePropertyChanged("MainColor"); }
        //}

        private Color _MainColor = Color.FromHex("#FDC1FB");
        public Color MainColor {
            get { return _MainColor; }
            set { _MainColor = value; RaisePropertyChanged("MainColor"); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #endregion

        #region Commands

        private RelayCommand _ParametresCommand;
        public RelayCommand ParametresCommand
        {
            get
            {
                return _ParametresCommand ?? (_ParametresCommand = new RelayCommand(ParametresAction));
            }
        }

        private RelayCommand _AccueilCommand;
        public RelayCommand AccueilCommand
        {
            get
            {
                return _AccueilCommand ?? (_AccueilCommand = new RelayCommand(AccueilAction));
            }
        }

        private RelayCommand _PanierCommand;
        public RelayCommand PanierCommand
        {
            get
            {
                return _PanierCommand ?? (_PanierCommand = new RelayCommand(PanierAction));
            }
        }

        private RelayCommand _RetourCommand;
        public RelayCommand RetourCommand
        {
            get
            {
                return _RetourCommand ?? (_RetourCommand = new RelayCommand(RetourAction));
            }
        }

        private RelayCommand _OpenBioRapideCommand;
        public RelayCommand OpenBioRapideCommand
        {
            get
            {
                return _OpenBioRapideCommand ?? (_OpenBioRapideCommand = new RelayCommand(OpenBioRapideAction));
            }
        }

        private RelayCommand _CloseBioRapideCommand;
        public RelayCommand CloseBioRapideCommand
        {
            get
            {
                return _CloseBioRapideCommand ?? (_CloseBioRapideCommand = new RelayCommand(CloseBioRapideAction));
            }
        }

        private RelayCommand _SearchCommand;
        public RelayCommand SearchCommand {
            get {
                return _SearchCommand ?? (_SearchCommand = new RelayCommand(SearchAction));
            }
        }

        private RelayCommand _BoxAVenirCommand;
        public RelayCommand BoxAVenirCommand
        {
            get
            {
                return _BoxAVenirCommand ?? (_BoxAVenirCommand = new RelayCommand(BoxAVenirAction));
            }
        }

        #endregion

        #region Methods

        public void PanierAction()
        {
            if (!this.IsBusy)
            {
                this.IsSearch = false;
                this.IsAccueil = false;
                this.IsBioRapide = false;
                this.IsParametre = false;
                this.IsPanier = true;
            }         
        }

        public async void BoxAVenirAction()
        {
            await Application.Current.MainPage.DisplayAlert("INFORMATIONS", "Compte premium à venir", "Compris");
        }

        public void ParametresAction()
        {
            if (!this.IsBusy)
            {
                this.IsSearch = false;
                this.IsAccueil = false;
                this.IsBioRapide = false;
                this.IsParametre = true;
                this.IsPanier = false;
            }           
        }

        public void AccueilAction()
        {
            if (!this.IsBusy)
            {
                this.IsSearch = false;
                this.IsAccueil = true;
                this.IsBioRapide = false;
                this.IsParametre = false;
                this.IsPanier = false;
            }
        }

        public void OpenBioRapideAction()
        {
            this.IsBioRapide = true;
            this.IsSearch = false;
            this.IsAccueil = false;
            this.IsParametre = false;
            this.IsPanier = false;
        }

        public void CloseBioRapideAction()
        {
            this.IsBioRapide = false;
            this.IsAccueil = true;
            this.IsSearch = false;
            this.IsBioRapide = false;
            this.IsParametre = false;
            this.IsPanier = false;
        }

        private void SearchAction() {
            if (!this.IsBusy) {
                this.IsAccueil = false;
                this.IsBioRapide = false;
                this.IsParametre = false;
                this.IsPanier = false;
                this.IsSearch = true;
            }
        }

        public async void RetourAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;
                await Application.Current.MainPage.Navigation.PopAsync();
                this.IsBusy = false;
            }          
        }

        public void GetSeason()
        {
            this.MainColor = Color.FromHex("#FFFFFF");
            var date = DateTime.Now;

            bool lastYearIsLeap = DateTime.IsLeapYear(date.Year - 1);
            bool thisIsLeap = DateTime.IsLeapYear(date.Year);
            bool nextYearIsLeap = DateTime.IsLeapYear(date.Year + 1);

            float summerStart = 6.21f;
            float autumnStart = 9.23f;
            float winterStart = 12.21f;

            if (thisIsLeap)
            {
                summerStart = 6.20f;
            }

            if (thisIsLeap || lastYearIsLeap)
            {
                autumnStart = 9.22f;
            }

            if (nextYearIsLeap)
            {
                winterStart = 12.22f;
            }

            if (date.Year == 2034 || date.Year == 2038)
                autumnStart -= 0.01f;

            float value = (float)date.Month + date.Day / 100f;   // <month>.<day(2 digit)>
            if (value < 3.20 || value >= winterStart)
            {
                this.MainColor = Color.FromHex("#B2ECFD");   // Winter
            }
            if (MainColor == Color.FromHex("#FFFFFF") && value < summerStart)
            {
                this.MainColor = Color.FromHex("#C4EF9F"); // Spring
            }
            if (MainColor == Color.FromHex("#FFFFFF") && value < autumnStart)
            {
                this.MainColor = Color.FromHex("#FDC1FB"); // Summer
            }
            else if (MainColor == Color.FromHex("#FFFFFF"))
            {
                this.MainColor = Color.FromHex("#FAD53A"); // Autumn
            }
        }

        //private void OpenTousAction()
        //{
        //    this.IsBusy = true;

        //    this.ProductDisplayedList = new ObservableCollection<DisplayItems>(this.ProductList);
        //    this.IsTous = true;
        //    this.IsBox = false;
        //    this.IsFruit = false;
        //    this.IsLegume = false;

        //    this.IsBusy = false;
        //}

        #endregion
    }
}
