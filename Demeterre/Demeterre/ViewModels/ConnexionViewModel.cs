﻿using Demeterre.Views;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Demeterre.ViewModels
{
    public class ConnexionViewModel : BaseViewModel
    {
        #region Properties

        private bool _IsConnexion;
        public bool IsConnexion
        {
            get { return _IsConnexion; }
            set { _IsConnexion = value; RaisePropertyChanged("IsConnexion"); }
        }

        private bool _IsNewAccount;
        public bool IsNewAccount
        {
            get { return _IsNewAccount; }
            set { _IsNewAccount = value; RaisePropertyChanged("IsNewAccount"); }
        }

        private string _NomComplet;
        public string NomComplet
        {
            get { return _NomComplet; }
            set { _NomComplet = value; RaisePropertyChanged("NomComplet"); }
        }

        private string _Mail;
        public string Mail
        {
            get { return _Mail; }
            set { _Mail = value; RaisePropertyChanged("Mail"); }
        }

        private string _Mdp;
        public string Mdp
        {
            get { return _Mdp; }
            set { _Mdp = value; RaisePropertyChanged("Mdp"); }
        }

        #endregion

        #region Command

        private RelayCommand _NewAccountCommand;
        public RelayCommand NewAccountCommand
        {
            get
            {
                return _NewAccountCommand ?? (_NewAccountCommand = new RelayCommand(NewAccountAction));
            }
        }

        private RelayCommand _ConnexionCommand;
        public RelayCommand ConnexionCommand
        {
            get
            {
                return _ConnexionCommand ?? (_ConnexionCommand = new RelayCommand(ConnexionAction));
            }
        }

        private RelayCommand _CheckIdCommand;
        public RelayCommand CheckIdCommand
        {
            get
            {
                return _CheckIdCommand ?? (_CheckIdCommand = new RelayCommand(CheckIdAction));
            }
        }

        #endregion

        #region Method

        public ConnexionViewModel()
        {
            InitView();
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.IsProfil = true;
            this.IsConnexion = false;
            this.IsNewAccount = false;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        public void LoadData()
        {
            //TODO
        }

        private void NewAccountAction()
        {
            this.IsNewAccount = true;
            this.IsConnexion = false;
            this.IsProfil = false;
        }

        private void ConnexionAction()
        {
            this.IsNewAccount = false;
            this.IsConnexion = true;
            this.IsProfil = false;
        }

        private async void CheckIdAction()
        {
            this.IsBusy = true;
            if (string.IsNullOrEmpty(this.Mail) || string.IsNullOrEmpty(this.Mdp)) {
                await Application.Current.MainPage.DisplayAlert("Attention", "Identifiant et/ou mot de passe incorrect", "Compris");
            } else {
                this.IsSuccess = true;
                await Task.Delay(1000);
                if (IsStudent) {
                    App.Locator.AccueilVM.InitView();
                    Page targetPage = (Page)Activator.CreateInstance(typeof(AccueilPage));
                    await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                } else {
                    App.Locator.ProducerHomeVM.InitView();
                    Page targetPage = (Page)Activator.CreateInstance(typeof(ProducerHomePage));
                    await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                }

                this.IsSuccess = false;
            }
            this.IsBusy = false;
        }
        #endregion
    }
}
