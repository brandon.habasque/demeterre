﻿using Demeterre.Models;
using Demeterre.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre.ViewModels
{
    public class DetailElementPourPanierViewModel : BaseViewModel
    {
        #region Properties

        private DisplayItems _SelectedProduit;
        public DisplayItems SelectedProduit
        {
            get { return _SelectedProduit; }
            set { _SelectedProduit = value; RaisePropertyChanged("SelectedProduit"); }
        }

        #endregion

        #region Command



        #endregion

        #region Methods
        public DetailElementPourPanierViewModel()
        {

        }

        public async void InitView()
        {
            this.IsBusy = true;

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {
            App.Locator.PanierVM.ProduitsPanier.Add(SelectedProduit);
            App.Locator.AccueilVM.CountProdPanier = App.Locator.PanierVM.ProduitsPanier.Count;
        }

        #endregion
    }
}
