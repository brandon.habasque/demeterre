﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre.ViewModels
{
    public class EditProfilViewModel : BaseViewModel
    {
        #region Properties


        #endregion

        #region Commands

        #endregion

        #region Methods
        public EditProfilViewModel()
        {
            this.InitView();
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {

        }

        #endregion

    }
}
