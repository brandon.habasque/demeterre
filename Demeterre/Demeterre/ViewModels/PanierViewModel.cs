﻿using Demeterre.Models;
using Demeterre.Views;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Demeterre.ViewModels
{
    public class PanierViewModel : BaseViewModel
    {
        #region Properties

        private ObservableCollection<DisplayItems> _ProduitsPanier;
        public ObservableCollection<DisplayItems> ProduitsPanier
        {
            get { return _ProduitsPanier; }
            set { _ProduitsPanier = value; RaisePropertyChanged("ProduitsPanier"); }
        }

        #endregion

        #region Commands

        private RelayCommand _PaiementCommand;
        public RelayCommand PaiementCommand
        {
            get
            {
                return _PaiementCommand ?? (_PaiementCommand = new RelayCommand(PaiementAction));
            }
        }


        #endregion

        #region Methods
        public PanierViewModel()
        {
            this.InitView();
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {
            this.ProduitsPanier = new ObservableCollection<DisplayItems>()
            {
                //new DisplayItems()
                //{
                //    ID = 0,
                //    Lib1 = "2 têtes",
                //    Lib2 = "legumes.png",
                //    Lib3 = "Brocolis",
                //    Lib4 = "0.80€"
                //},
                //new DisplayItems()
                //{
                //    ID = 1,
                //    Lib1 = "300g",
                //    Lib2 = "agrumes.png",
                //    Lib3 = "Agrumes",
                //    Lib4 = "3.00€"
                //},
                //new DisplayItems()
                //{
                //    ID = 2,
                //    Lib1 = "5",
                //    Lib2 = "exotiques.png",
                //    Lib3 = "Bananes",
                //    Lib4 = "1.50€"
                //},
                //new DisplayItems()
                //{
                //    ID = 3,
                //    Lib1 = "2 boîtes",
                //    Lib2 = "baies.png",
                //    Lib3 = "Fraises",
                //    Lib4 = "4.00€"
                //}
            };
           
        }

        public async void PaiementAction()
        {
            if (!this.IsBusy && this.ProduitsPanier.Count > 0)
            {
                App.Locator.RecapPanierVM.InitView();
                this.IsBusy = true;
                Page targetPage = (Page)Activator.CreateInstance(typeof(RecapPanierPage));
                await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                this.IsBusy = false;
            } else {
                await Application.Current.MainPage.DisplayAlert("Impossible", "Votre panier est vide", "Compris");
            }
     
        } 

        #endregion
    }
}
