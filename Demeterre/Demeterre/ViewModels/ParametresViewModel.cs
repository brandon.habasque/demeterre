﻿using Demeterre.Views;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Demeterre.ViewModels
{
    public class ParametresViewModel : BaseViewModel
    {
        #region Properties


        #endregion

        #region Commands

        private RelayCommand _LoadProfilCommand;
        public RelayCommand LoadProfilCommand
        {
            get
            {
                return _LoadProfilCommand ?? (_LoadProfilCommand = new RelayCommand(LoadProfilAction));
            }
        }

        private RelayCommand _LoadHistoriqueCommand;
        public RelayCommand LoadHistoriqueCommand
        {
            get
            {
                return _LoadHistoriqueCommand ?? (_LoadHistoriqueCommand = new RelayCommand(LoadHistoriqueCommandAction));
            }
        }

        #endregion

        #region Methods
        public ParametresViewModel()
        {
            this.InitView();
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {

        }

        public async void LoadProfilAction()
        {
            if (!this.IsBusy)
            {
                App.Locator.EditProfilVM.InitView();
                this.IsBusy = true;
                Page targetPage = (Page)Activator.CreateInstance(typeof(EditProfilPage));
                await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                this.IsBusy = false;
            }           
        }

        public async void LoadCommandeAction() {
            await Application.Current.MainPage.DisplayAlert("Information", "Il n'y a aucune commande à afficher", "Compris");
        }

        private async void LoadHistoriqueCommandAction()
        {
            if (!this.IsBusy)
            {
                //App.Locator.EditProfilVM.InitView();
                this.IsBusy = true;
                //Page targetPage = (Page)Activator.CreateInstance(typeof(EditProfilPage));
               // await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                this.IsBusy = false;
            }
        }

        public async void DeconnexionAction() {
            bool isOk = await Application.Current.MainPage.DisplayAlert("Attention", "Souhaitez-vous vous déconnecter de l'application ?", "Oui", "Non");
            if (isOk) {
                App.Locator.ConnexionVM.InitView();
                this.IsBusy = true;
                Page targetPage = (Page)Activator.CreateInstance(typeof(ConnexionPage));
                await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                this.IsBusy = false;
            }
        }

        #endregion
    }
}
