﻿using Demeterre_Library.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using Newtonsoft.Json.Schema;

namespace Demeterre.ViewModels
{
    public class ProducerHomeViewModel : BaseViewModel
    {
        #region properties
        private ObservableCollection<CommandeMob> _CommandesList; 
        public ObservableCollection<CommandeMob> CommandesList {
            get { return _CommandesList; }
            set { _CommandesList = value; RaisePropertyChanged("CommandesList"); }
        }

        private ObservableCollection<ProduitMob> _ProduitList; 
        public ObservableCollection<ProduitMob> ProduitList {
            get { return _ProduitList; }
            set { _ProduitList = value; RaisePropertyChanged("ProduitList"); }
        }

        private ProduitMob _SelectedProduit;
        public ProduitMob SelectedProduit {
            get { return _SelectedProduit; }
            set {
                _SelectedProduit = value; if (value != null) {
                    this.GoToFicheProduit();
                } RaisePropertyChanged("SelectedProduit"); }
        }

        private bool _IsSelectedProd;
        public bool IsSelectedProd {
            get { return _IsSelectedProd; }
            set { _IsSelectedProd = value; RaisePropertyChanged("IsSelectedProd"); }
        }

        private bool _IsFruit;
        public bool IsFruit {
            get { return _IsFruit; }
            set {
                _IsFruit = value; if (value) {
                    this.IsLegume = false;
                } RaisePropertyChanged("IsFruit"); }
        }

        private bool _IsLegume;
        public bool IsLegume {
            get { return _IsLegume; }
            set {
                _IsLegume = value; if (value) {
                    this.IsFruit = false;
                } RaisePropertyChanged("IsLegume"); }
        }

        private string _ProduitNom;
        public string ProduitNom {
            get { return _ProduitNom; }
            set { _ProduitNom = value; RaisePropertyChanged("ProduitNom"); }
        }

        private string _ProduitQte;
        public string ProduitQte {
            get { return _ProduitQte; }
            set { _ProduitQte = value; RaisePropertyChanged("ProduitQte"); }
        }

        private string _ProduitAdresse;
        public string ProduitAdresse {
            get { return _ProduitAdresse; }
            set { _ProduitAdresse = value; RaisePropertyChanged("ProduitAdresse"); }
        }

        private DateTime _ProduitDateLimite;
        public DateTime ProduitDateLimite {
            get { return _ProduitDateLimite; }
            set { _ProduitDateLimite = value; RaisePropertyChanged("ProduitDateLimite"); }
        }

        private string _ProduitPrix;
        public string ProduitPrix {
            get { return _ProduitPrix; }
            set { _ProduitPrix = value; RaisePropertyChanged("ProduitPrix"); }
        }

        #endregion

        private RelayCommand _ModifierCommand;
        public RelayCommand ModifierCommand {
            get {
                return _ModifierCommand ?? (_ModifierCommand = new RelayCommand(ModifierAction));
            }
        }

        private RelayCommand _SupprimerCommand;
        public RelayCommand SupprimerCommand {
            get {
                return _SupprimerCommand ?? (_SupprimerCommand = new RelayCommand(SupprimerAction));
            }
        }

        private RelayCommand _RetourProduitCommand;
        public RelayCommand RetourProduitCommand {
            get {
                return _RetourProduitCommand ?? (_RetourProduitCommand = new RelayCommand(RetourProduitAction));
            }
        }

        private RelayCommand _ParametresProdCommand;
        public RelayCommand ParametresProdCommand {
            get {
                return _ParametresProdCommand ?? (_ParametresProdCommand = new RelayCommand(this.ParametresAction));
            }
        }

        private RelayCommand _EnregistrerCommand;
        public RelayCommand EnregistrerCommand {
            get {
                return _EnregistrerCommand ?? (_EnregistrerCommand = new RelayCommand(this.EnregistrerAction));
            }
        }

        public ProducerHomeViewModel()
        {
            this.InitView();
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.IsAccueil = true;
            this.IsPanier = false;
            this.IsBioRapide = false;
            this.IsParametre = false;
            this.IsSearch = false;
            this.IsFruit = true;
            this.IsLegume = false;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {
            this.CommandesList = new ObservableCollection<CommandeMob>();
            this.CommandesList.Add(new CommandeMob()
            {
                DateCommande = DateTime.Now,
                TiersCommande = new TiersMob()
                {
                    NomTiers = "Samuel Garnier",
                },
                Produits = new List<ProduitMob>()
                {
                    new ProduitMob()
                    {
                        LibelleProduit = "Tomates",
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Poivron"
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Aubergine"
                    }
                },
                nbProduits = 3
            });
            this.CommandesList.Add(new CommandeMob()
            {
                DateCommande = DateTime.Now,
                TiersCommande = new TiersMob()
                {
                    NomTiers = "Eloïse Coignat",
                },
                Produits = new List<ProduitMob>()
                {
                    new ProduitMob()
                    {
                        LibelleProduit = "Fraise",
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Poire"
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Pomme"
                    }
                },
                nbProduits = 3
            });
            this.CommandesList.Add(new CommandeMob()
            {
                DateCommande = DateTime.Now,
                TiersCommande = new TiersMob()
                {
                    NomTiers = "Brandon Habasque",
                },
                Produits = new List<ProduitMob>()
                {
                    new ProduitMob()
                    {
                        LibelleProduit = "Litchi",
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Orange"
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Banane"
                    }
                },
                nbProduits = 3
            });
            this.CommandesList.Add(new CommandeMob()
            {
                DateCommande = DateTime.Now,
                TiersCommande = new TiersMob()
                {
                    NomTiers = "Quentin Magnier",
                },
                Produits = new List<ProduitMob>()
                {
                    new ProduitMob()
                    {
                        LibelleProduit = "Courgette",
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Pamplemousse"
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Patate douce"
                    }
                },
                nbProduits = 3
            });
            this.CommandesList.Add(new CommandeMob()
            {
                DateCommande = DateTime.Now,
                TiersCommande = new TiersMob()
                {
                    NomTiers = "Dylan Louvel",
                },
                Produits = new List<ProduitMob>()
                {
                    new ProduitMob()
                    {
                        LibelleProduit = "Salade",
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Concombre"
                    },
                    new ProduitMob()
                    {
                        LibelleProduit = "Melon"
                    }
                },
                nbProduits = 3
            });

            this.ProduitList = new ObservableCollection<ProduitMob>();
            this.ProduitList.Add(new ProduitMob() {
                IDProduit = 0,
                LibelleProduit = "Laitue",
                QuantiteStock = 20,
                QuantiteVendu = 0,
                TypeProduit = "Légume",
                Image = "LaitueColor.png"
            });
            this.ProduitList.Add(new ProduitMob() {
                IDProduit = 0,
                LibelleProduit = "Tomate",
                QuantiteStock = 50,
                QuantiteVendu = 0,
                TypeProduit = "Légume",
                Image = "TomateColor.png"
            });
            this.ProduitList.Add(new ProduitMob() {
                IDProduit = 0,
                LibelleProduit = "Pomme",
                QuantiteStock = 50,
                QuantiteVendu = 0,
                TypeProduit = "Fruit",
                Image = "appleColor.png"
            });
            this.ProduitList.Add(new ProduitMob() {
                IDProduit = 0,
                LibelleProduit = "Kiwi",
                QuantiteStock = 110,
                QuantiteVendu = 0,
                TypeProduit = "Fruit",
                Image = "kiwiColor.png"
            });
        }

        private void GoToFicheProduit() {
            this.IsSearch = true;
            this.IsPanier = false;
        }

        private async void ModifierAction() {
            await Application.Current.MainPage.DisplayAlert("MODIFIER", "Modification d'un produit bientôt disponible", "Compris");
        }

        private void SupprimerAction() {
            this.ProduitList.Remove(this.SelectedProduit);
            this.IsPanier = true;
            this.IsSearch = false;
        }

        private void RetourProduitAction() {
            this.IsPanier = true;
            this.IsSearch = false;
            this.SelectedProduit = null;
        }

        public new async void ParametresAction(){
            if (!this.IsBusy) {
                await Application.Current.MainPage.DisplayAlert("PARAMETRES", "Les paramètres seront bientôt disponibles", "compris");
            }
        }

        private async void EnregistrerAction() {
            if (!string.IsNullOrEmpty(this.ProduitNom) && !string.IsNullOrEmpty(this.ProduitQte)) {
                ProduitMob myProduit = new ProduitMob() {
                    LibelleProduit = this.ProduitNom,
                    QuantiteStock = int.Parse(this.ProduitQte),
                    Image = "search.png",
                    TypeProduit = this.IsFruit ? "Fruit" : "Légume"
                };

                this.ProduitList.Add(myProduit);
                this.IsBioRapide = false;
                this.IsPanier = true;

                this.IsFruit = true;
                this.ProduitAdresse = string.Empty;
                this.ProduitNom = string.Empty;
                this.ProduitDateLimite = DateTime.Now;
                this.ProduitQte = string.Empty;
                this.ProduitPrix = string.Empty;
            } else {
                await Application.Current.MainPage.DisplayAlert("Action impossible", "il manque une ou des informations", "Compris");
            }
        }
    }
}
