﻿using Demeterre.Models;
using Demeterre.Views;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Demeterre.ViewModels
{
    public class RecapPanierViewModel : BaseViewModel
    {
        #region Properties

        private bool _IsResume;
        public bool IsResume
        {
            get { return _IsResume; }
            set { _IsResume = value; RaisePropertyChanged("IsResume"); }
        }

        private bool _IsLivraison;
        public bool IsLivraison
        {
            get { return _IsLivraison; }
            set { _IsLivraison = value; RaisePropertyChanged("IsLivraison"); }
        }

        private bool _IsPointRelai;
        public bool IsPointRelai
        {
            get { return _IsPointRelai; }
            set { _IsPointRelai = value; RaisePropertyChanged("IsPointRelai"); }
        }

        private bool _IsReception;
        public bool IsReception
        {
            get { return _IsReception; }
            set { _IsReception = value; RaisePropertyChanged("IsReception"); }
        }

        private bool _IsPayement;
        public bool IsPayement
        {
            get { return _IsPayement; }
            set { _IsPayement = value; RaisePropertyChanged("IsPayement"); }
        }

        private bool _IsList;
        public bool IsList
        {
            get { return _IsList; }
            set { _IsList = value; RaisePropertyChanged("IsList"); }
        }

        private bool _IsCommandeConfirme;
        public bool IsCommandeConfirme
        {
            get { return _IsCommandeConfirme; }
            set { _IsCommandeConfirme = value; RaisePropertyChanged("IsCommandeConfirme"); }
        }        
        
        private bool _IsNotCommandeConfirme;
        public bool IsNotCommandeConfirme
        {
            get { return _IsNotCommandeConfirme; }
            set { _IsNotCommandeConfirme = value; RaisePropertyChanged("IsNotCommandeConfirme"); }
        }

        private ObservableCollection<DisplayItems> _ListProdPanier;
        public ObservableCollection<DisplayItems> ListProdPanier
        {
            get { return _ListProdPanier; }
            set { _ListProdPanier = value; RaisePropertyChanged("ListProdPanier"); }
        }

        #endregion

        #region Commands

        private RelayCommand _OpenResumeCommand;
        public RelayCommand OpenResumeCommand
        {
            get
            {
                return _OpenResumeCommand ?? (_OpenResumeCommand = new RelayCommand(OpenResumeAction));
            }
        }

        private RelayCommand _OpenLivraisonCommand;
        public RelayCommand OpenLivraisonCommand
        {
            get
            {
                return _OpenLivraisonCommand ?? (_OpenLivraisonCommand = new RelayCommand(OpenLivraisonAction));
            }
        }

        private RelayCommand _OpenPayementCommand;
        public RelayCommand OpenPayementCommand
        {
            get
            {
                return _OpenPayementCommand ?? (_OpenPayementCommand = new RelayCommand(OpenPayementAction));
            }
        }

        private RelayCommand _OpenListCommand;
        public RelayCommand OpenListCommand
        {
            get
            {
                return _OpenListCommand ?? (_OpenListCommand = new RelayCommand(OpenListAction));
            }
        }

        private RelayCommand _OpenConfirmeCommand;
        public RelayCommand OpenConfirmeCommand
        {
            get
            {
                return _OpenConfirmeCommand ?? (_OpenConfirmeCommand = new RelayCommand(OpenConfirmeAction));
            }
        }

        private RelayCommand _OpenPaypalCommand;
        public RelayCommand OpenPaypalCommand
        {
            get
            {
                return _OpenPaypalCommand ?? (_OpenPaypalCommand = new RelayCommand(OpenPaypalAction));
            }
        }

        private RelayCommand _QuitterCommand;
        public RelayCommand QuitterCommand
        {
            get
            {
                return _QuitterCommand ?? (_QuitterCommand = new RelayCommand(QuitterAction));
            }
        }


        #endregion

        #region Methods
        public RecapPanierViewModel()
        {
            
        }

        public void InitView()
        {
            this.IsBusy = true;

            this.IsResume = true;
            this.IsLivraison = false;
            this.IsPointRelai = false;
            this.IsReception = true;
            this.IsList = false;
            this.IsPayement = false;
            this.IsCommandeConfirme = false;
            this.IsNotCommandeConfirme = true;

            this.GetSeason();

            this.LoadData();

            this.IsBusy = false;
        }

        private void LoadData()
        {
            this.ListProdPanier = App.Locator.PanierVM.ProduitsPanier;
            this.NumCommande = 1;
        }

        private async void QuitterAction()
        {
            if (!this.IsBusy)
            {
                App.Locator.AccueilVM.InitView();
                App.Locator.PanierVM.ProduitsPanier.Clear();
                this.IsBusy = true;
                Page targetPage = (Page)Activator.CreateInstance(typeof(AccueilPage));
                await Application.Current.MainPage.Navigation.PushAsync(targetPage);
                this.IsBusy = false;
            }
        }

        private void OpenResumeAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;

                this.IsResume = true;
                this.IsLivraison = false;
                this.IsList = false;
                this.IsPayement = false;
                this.IsCommandeConfirme = false;
                this.IsNotCommandeConfirme = true;

                this.IsBusy = false;
            }          
        }

        private void OpenLivraisonAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;

                this.IsResume = false;
                this.IsLivraison = true;
                this.IsList = false;
                this.IsPayement = false;
                this.IsCommandeConfirme = false;
                this.IsNotCommandeConfirme = true;

                this.IsBusy = false;
            }            
        }
        
        private void OpenPayementAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;

                this.IsResume = false;
                this.IsLivraison = false;
                this.IsList = false;
                this.IsPayement = true;
                this.IsCommandeConfirme = false;
                this.IsNotCommandeConfirme = true;

                this.IsBusy = false;
            }
        }

        private void OpenListAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;

                this.IsResume = false;
                this.IsLivraison = false;
                this.IsList = true;
                this.IsPayement = false;
                this.IsCommandeConfirme = false;
                this.IsNotCommandeConfirme = true;

                this.IsBusy = false;
            }            
        }

        private void OpenConfirmeAction()
        {
            if (!this.IsBusy)
            {
                this.IsBusy = true;

                this.IsResume = false;
                this.IsLivraison = false;
                this.IsList = false;
                this.IsPayement = false;
                this.IsCommandeConfirme = true;
                this.IsNotCommandeConfirme = false;

                this.IsBusy = false;
            }
        }

        private void OpenPaypalAction()
        {
            OpenConfirmeAction();
        }
        #endregion
    }
}
