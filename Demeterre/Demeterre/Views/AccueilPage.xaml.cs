﻿using Demeterre.Models;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccueilPage : ContentPage
    {
        public AccueilPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = App.Locator.AccueilVM;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Locator.AccueilVM.RetourAction();
            return !base.OnBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Locator.AccueilVM.IsBusy = false;
        }

        private async void IconButton_Clicked(object sender, EventArgs e)
        {
            panier.Opacity = 0;
            parametre.Opacity = 0;
            search.Opacity = 0;
            await map.FadeTo(1, 600);
            //App.Locator.AccueilVM.OpenBioRapideAction();
        }

        private async void IconButton_Clicked_1(object sender, EventArgs e)
        {
            panier.Opacity = 0;
            parametre.Opacity = 0;
            search.Opacity = 0;
            await map.FadeTo(0, 600);
            App.Locator.AccueilVM.CloseBioRapideAction();
            App.Locator.AccueilVM.IsSelectedProd = false;
            App.Locator.AccueilVM.SelectedPin = null;
        }

        private async void IconButton_Clicked_2(object sender, EventArgs e)
        {
            map.Opacity = 0;
            parametre.Opacity = 0;
            search.Opacity = 0;
            await panier.FadeTo(1, 600);
            //App.Locator.AccueilVM.PanierAction();
        }

        private async void IconButton_Clicked_3(object sender, EventArgs e)
        {
            map.Opacity = 0;
            panier.Opacity = 0;
            search.Opacity = 0;
            await parametre.FadeTo(1, 600);
            //App.Locator.AccueilVM.ParametresAction();
        }

        private async void IconButton_Clicked_4(object sender, EventArgs e) {
            map.Opacity = 0;
            panier.Opacity = 0;
            parametre.Opacity = 0;
            await search.FadeTo(1, 600);
        }

        private async void IconButton_Clicked_5(object sender, EventArgs e) {
            map.Opacity = 0;
            panier.Opacity = 0;
            parametre.Opacity = 0;
            await search.FadeTo(0, 600);
            App.Locator.AccueilVM.SearchValue = string.Empty;
            App.Locator.AccueilVM.IsSearch = false;
            App.Locator.AccueilVM.IsAccueil = true;
        }

        private async void ExtendedMap_MapClicked(object sender, Xamarin.Forms.Maps.MapClickedEventArgs e) {
            var pins = App.Locator.AccueilVM.PinCollection;
            foreach (var item in pins) {
                if (e.Position == item.Position) {
                    bool isOk = await DisplayAlert(item.Label, "Voulez-vous sélectionner ce producteur ?", "Oui", "Non");
                    if (isOk) {
                        App.Locator.AccueilVM.IsSelectedProd = true;
                    }
                }
            }
        }
    }
}