﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnexionPage : ContentPage
    {
        public ConnexionPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = App.Locator.ConnexionVM;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Locator.ConnexionVM.RetourAction();
            return !base.OnBackButtonPressed();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            profil.Opacity = 0;
            App.Locator.ConnexionVM.IsStudent = true;
            App.Locator.ConnexionVM.IsProducer = false;
            await connexion.FadeTo(1, 600);
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            profil.Opacity = 0;
            App.Locator.ConnexionVM.IsStudent = false;
            App.Locator.ConnexionVM.IsProducer = true;
            await connexion.FadeTo(1, 600);
        }

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            account.Opacity = 0;
            await connexion.FadeTo(1, 600);
        }

        private async void Button_Clicked_3(object sender, EventArgs e)
        {
            connexion.Opacity = 0;
            await account.FadeTo(1, 600);
        }
    }
}