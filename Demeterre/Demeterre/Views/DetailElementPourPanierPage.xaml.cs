﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailElementPourPanierPage : ContentPage
    {
        public DetailElementPourPanierPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = App.Locator.DetailElementPourPanierVM;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Locator.DetailElementPourPanierVM.RetourAction();
            return !base.OnBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Locator.DetailElementPourPanierVM.IsBusy = false;
        }

        private void Clicked_AjoutPanier(object sender, EventArgs e)
        {
            App.Locator.DetailElementPourPanierVM.RetourAction();
        }
    }
}