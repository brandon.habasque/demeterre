﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilPage : ContentPage
    {
        public EditProfilPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = App.Locator.EditProfilVM;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Locator.EditProfilVM.IsBusy = false;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Locator.EditProfilVM.RetourAction();
            return !base.OnBackButtonPressed();          
        }
    }
}