﻿using Demeterre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PanierView : ContentView
    {
        private PanierViewModel Panier = App.Locator.PanierVM;
        public PanierView()
        {
            InitializeComponent();
        }

        private void Clicked_Paiement(object sender, EventArgs e)
        {
            Panier.PaiementAction();
        }
    }
}