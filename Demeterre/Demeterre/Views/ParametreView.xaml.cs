﻿using Demeterre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ParametreView : ContentView
    {
        private ParametresViewModel Parametre = App.Locator.ParametresVM;
        public ParametreView()
        {
            InitializeComponent();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Parametre.LoadProfilAction();
        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e) {
            Parametre.DeconnexionAction();
        }

        private void TapGestureRecognizer_Tapped_2(object sender, EventArgs e) {
            Parametre.LoadCommandeAction();
        }
    }
}