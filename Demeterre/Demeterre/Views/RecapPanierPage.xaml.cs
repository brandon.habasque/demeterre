﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Demeterre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecapPanierPage : ContentPage
    {
        public RecapPanierPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = App.Locator.RecapPanierVM;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Locator.RecapPanierVM.RetourAction();
            return !base.OnBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Locator.RecapPanierVM.IsBusy = false;
        }

        private void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value)
            {
                App.Locator.RecapPanierVM.IsReception = false;
            }
        }

        private void CheckBox_CheckedChanged_1(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value)
            {
                App.Locator.RecapPanierVM.IsPointRelai = false;
            }
        }
    }
}