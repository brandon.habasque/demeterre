﻿using Demeterre_Library.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre_Library
{
    public class DownloadMobileContext
    {
        public string messageRetour { get; set; }

        public TiersMob Tiers { get; set; }

        public List<ProduitMob> Produits { get; set; }

        public List<CommandeMob> Commandes { get; set; }

        public List<PointRelaisMob> PointsRelais { get; set; }
    }
}
