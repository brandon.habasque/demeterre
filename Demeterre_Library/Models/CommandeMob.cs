﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre_Library.Models
{
    public class CommandeMob
    {
        public int IDCommande { get; set; }
        public DateTime DateCommande { get; set; }
        public List<ProduitMob> Produits { get; set; }
        public TiersMob TiersCommande { get; set; }
        public PointRelaisMob PointRelaisCommande { get; set; }
        public int nbProduits { get; set; }

    }
}
