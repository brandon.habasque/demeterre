﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre_Library.Models
{
    public class PointRelaisMob
    {
        public int IDPointRelais { get; set; }
        public string AdrRue { get; set; }
        public string AdrVille { get; set; }
        public string AdrCP { get; set; }
    }
}
