﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre_Library.Models
{
    public class ProduitMob
    {
        public int IDProduit { get; set; }
        public string TypeProduit { get; set; }
        public string LibelleProduit { get; set; }
        public int QuantiteVendu { get; set; }
        public int QuantiteStock { get; set; }
        public string Image { get; set; }
    }
}
