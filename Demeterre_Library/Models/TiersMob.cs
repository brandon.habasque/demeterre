﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demeterre_Library.Models
{
    public class TiersMob
    {
        public int IDTiers { get; set; }
        public string TypeTiers { get; set; }
        public string NomTiers { get; set; }
        public string PrenomTiers { get; set; }
        public string MailTiers { get; set; }
        public string AdrRueTiers { get; set; }
        public string AdrVilleTiers { get; set; }
        public string AdrCPTiers { get; set; }
    }
}
