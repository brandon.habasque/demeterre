﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class Commande
    {
        public int Id { get; set; }
        public int? IdTiers { get; set; }
        public int? IdPointRelais { get; set; }
        public DateTime? Date { get; set; }

        public virtual PointRelais IdPointRelaisNavigation { get; set; }
        public virtual Tiers IdTiersNavigation { get; set; }
    }
}
