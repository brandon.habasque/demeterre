﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class CommandeProduit
    {
        public int? IdProduit { get; set; }
        public int? IdCommande { get; set; }

        public virtual Commande IdCommandeNavigation { get; set; }
        public virtual Produit IdProduitNavigation { get; set; }
    }
}
