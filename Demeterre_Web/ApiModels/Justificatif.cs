﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class Justificatif
    {
        public int Id { get; set; }
        public int IdTiers { get; set; }
        public string DocPath { get; set; }
        public bool? IsValide { get; set; }

        public virtual Tiers IdTiersNavigation { get; set; }
    }
}
