﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class PointRelais
    {
        public PointRelais()
        {
            Commande = new HashSet<Commande>();
        }

        public int Id { get; set; }
        public string Libelle { get; set; }
        public string AdrRue { get; set; }
        public string AdrVille { get; set; }
        public string AdrCp { get; set; }
        public string Telephone { get; set; }

        public virtual ICollection<Commande> Commande { get; set; }
    }
}
