﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class Produit
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public int QuantiteVendue { get; set; }
        public int QuantiteStock { get; set; }
        public int IdTypeProduit { get; set; }

        public virtual TypeProduit IdTypeProduitNavigation { get; set; }
    }
}
