﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class Tiers
    {
        public Tiers()
        {
            Commande = new HashSet<Commande>();
            Justificatif = new HashSet<Justificatif>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Mail { get; set; }
        public string Mdp { get; set; }
        public int IdTypeTiers { get; set; }
        public string AdrRue { get; set; }
        public string AdrVille { get; set; }
        public string AdrCp { get; set; }
        public string Telephone { get; set; }

        public virtual TypeTiers IdTypeTiersNavigation { get; set; }
        public virtual ICollection<Commande> Commande { get; set; }
        public virtual ICollection<Justificatif> Justificatif { get; set; }
    }
}
