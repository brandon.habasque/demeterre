﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class TypeProduit
    {
        public TypeProduit()
        {
            Produit = new HashSet<Produit>();
        }

        public int Id { get; set; }
        public string Libelle { get; set; }

        public virtual ICollection<Produit> Produit { get; set; }
    }
}
