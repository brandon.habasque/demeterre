﻿using System;
using System.Collections.Generic;

namespace Demeterre_Web.ApiModels
{
    public partial class TypeTiers
    {
        public TypeTiers()
        {
            Tiers = new HashSet<Tiers>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }

        public virtual ICollection<Tiers> Tiers { get; set; }
    }
}
