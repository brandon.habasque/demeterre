﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Demeterre_Web.ApiModels
{
    public partial class demeterre_bddContext : DbContext
    {
        public demeterre_bddContext()
        {
        }

        public demeterre_bddContext(DbContextOptions<demeterre_bddContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Commande> Commande { get; set; }
        public virtual DbSet<CommandeProduit> CommandeProduit { get; set; }
        public virtual DbSet<Justificatif> Justificatif { get; set; }
        public virtual DbSet<PointRelais> PointRelais { get; set; }
        public virtual DbSet<Produit> Produit { get; set; }
        public virtual DbSet<Tiers> Tiers { get; set; }
        public virtual DbSet<TypeProduit> TypeProduit { get; set; }
        public virtual DbSet<TypeTiers> TypeTiers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:vps783151.ovh.net,1431;Database=demeterre_bdd;Trusted_Connection=True;Integrated Security=false;user id=sa;password=SqlDevOps2020;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Commande>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPointRelais).HasColumnName("idPointRelais");

                entity.Property(e => e.IdTiers).HasColumnName("idTiers");

                entity.HasOne(d => d.IdPointRelaisNavigation)
                    .WithMany(p => p.Commande)
                    .HasForeignKey(d => d.IdPointRelais)
                    .HasConstraintName("FK_Commande_PointRelais");

                entity.HasOne(d => d.IdTiersNavigation)
                    .WithMany(p => p.Commande)
                    .HasForeignKey(d => d.IdTiers)
                    .HasConstraintName("FK_Commande_Tiers");
            });

            modelBuilder.Entity<CommandeProduit>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.IdCommande).HasColumnName("idCommande");

                entity.Property(e => e.IdProduit).HasColumnName("idProduit");

                entity.HasOne(d => d.IdCommandeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdCommande)
                    .HasConstraintName("FK_CommandeProduit_Commande1");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdProduit)
                    .HasConstraintName("FK_CommandeProduit_Produit1");
            });

            modelBuilder.Entity<Justificatif>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DocPath)
                    .HasColumnName("docPath")
                    .HasMaxLength(120);

                entity.Property(e => e.IdTiers).HasColumnName("idTiers");

                entity.Property(e => e.IsValide).HasColumnName("isValide");

                entity.HasOne(d => d.IdTiersNavigation)
                    .WithMany(p => p.Justificatif)
                    .HasForeignKey(d => d.IdTiers)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Justificatif_Tiers");
            });

            modelBuilder.Entity<PointRelais>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdrCp)
                    .IsRequired()
                    .HasColumnName("adrCP")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.AdrRue)
                    .IsRequired()
                    .HasColumnName("adrRue")
                    .HasMaxLength(150);

                entity.Property(e => e.AdrVille)
                    .IsRequired()
                    .HasColumnName("adrVille")
                    .HasMaxLength(80);

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasColumnName("libelle")
                    .HasMaxLength(50);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Produit>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdTypeProduit).HasColumnName("idTypeProduit");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasColumnName("libelle")
                    .HasMaxLength(50);

                entity.Property(e => e.QuantiteStock).HasColumnName("quantiteStock");

                entity.Property(e => e.QuantiteVendue).HasColumnName("quantiteVendue");

                entity.HasOne(d => d.IdTypeProduitNavigation)
                    .WithMany(p => p.Produit)
                    .HasForeignKey(d => d.IdTypeProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Produit_TypeProduit");
            });

            modelBuilder.Entity<Tiers>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdrCp)
                    .HasColumnName("adrCP")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.AdrRue)
                    .HasColumnName("adrRue")
                    .HasMaxLength(150);

                entity.Property(e => e.AdrVille)
                    .HasColumnName("adrVille")
                    .HasMaxLength(50);

                entity.Property(e => e.IdTypeTiers).HasColumnName("idTypeTiers");

                entity.Property(e => e.Mail)
                    .IsRequired()
                    .HasColumnName("mail")
                    .HasMaxLength(100);

                entity.Property(e => e.Mdp)
                    .IsRequired()
                    .HasColumnName("mdp")
                    .HasMaxLength(50);

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasMaxLength(50);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(50);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.HasOne(d => d.IdTypeTiersNavigation)
                    .WithMany(p => p.Tiers)
                    .HasForeignKey(d => d.IdTypeTiers)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tiers_TypeTiers");
            });

            modelBuilder.Entity<TypeProduit>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasColumnName("libelle")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TypeTiers>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.Libelle)
                    .HasColumnName("libelle")
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
