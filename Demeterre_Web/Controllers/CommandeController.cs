﻿using Demeterre_Web.ApiModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeterre_Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommandeController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Commande> GetAll()
        {
            using (var context = new demeterre_bddContext())
            {
                return context.Commande.ToList();
            };
        }

    }
}
