﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demeterre_Library.Models;
using Demeterre_Web.ApiModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Demeterre_Web.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ProduitController : ControllerBase {

        [HttpGet]
        public IEnumerable<Produit> GetAll() {
            using (var context = new demeterre_bddContext()) {
                return context.Produit.ToList();
            };
        }

        [HttpGet]
        public Produit Get(int id)
        {
            using (var context = new demeterre_bddContext())
            {
                var produit = context.Produit.SingleOrDefault(o => o.Id == id);
                return produit;
            };            
        }

        [HttpGet]
        public void Add([FromBody] Produit produit)
        {
            using (var context = new demeterre_bddContext())
            {
                context.Produit.Add(produit);
                context.SaveChanges();
            };
        }

        [HttpGet]
        public void Remove([FromBody] Produit produit)
        {
            using (var context = new demeterre_bddContext())
            {
                context.Produit.Remove(produit);
                context.SaveChanges();
            };
        }

    }
}
