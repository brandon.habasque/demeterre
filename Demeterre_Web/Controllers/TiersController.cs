﻿using Demeterre_Web.ApiModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeterre_Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TiersController : ControllerBase
    {
        [HttpGet]
        public Tiers Get(int id)
        {
            using (var context = new demeterre_bddContext())
            {
                var tiers = context.Tiers.SingleOrDefault(o => o.Id == id);
                return tiers;
            };
        }

        [HttpGet]
        public void Update(Tiers tiersToUpdate, Tiers tiers)
        {
            using (var context = new demeterre_bddContext())
            {
                tiersToUpdate.Nom = tiers.Nom;
                tiersToUpdate.Prenom = tiers.Prenom;
                tiersToUpdate.AdrRue = tiers.AdrRue;
                tiersToUpdate.AdrCp = tiers.AdrCp;
                tiersToUpdate.AdrVille = tiers.AdrVille;
                tiersToUpdate.Telephone = tiers.Telephone;
                tiersToUpdate.Mail = tiers.Mail;

                context.SaveChanges();
            };
        }
    }
}
